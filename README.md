## MEAN Admin - Admin panel using MEAN (MongoDB, ExpressJS, AngularJS, Node.js)
This project is based on Free Admin Bootstrap Theme [SB Admin v2.0](http://startbootstrap.com/template-overviews/sb-admin-2/) to MEAN stack.
## Installation

1. Clone this project or Download that ZIP file
2. Make sure you have bower, grunt-cli and npm installed globally
3. On the command prompt run the following commands
- cd `project-directory`
- `npm install`
- cd `client`
- `bower install`
- `npm start`
