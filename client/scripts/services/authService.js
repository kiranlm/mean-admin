var login = angular.module('AuthSrvc',[]);
    login.factory('AuthService', ['$q', '$timeout', '$http','SessionService',
      function ($q, $timeout, $http,SessionService) {

    // create user variable
    var user = sessionStorage.getItem('auth');

    // return available functions for use in controllers
    return ({
      isLoggedIn: isLoggedIn,
      getUserStatus: getUserStatus,
      login: login,
      logout: logout,
      register: register
    });
function isLoggedIn() {
  if(user) {
    return true;
  } else {
    return false;
  }
}
function getUserStatus() {
  return user;
}

function login(username, password) {

  // create a new instance of deferred
  var deferred = $q.defer();

  // send a post request to the server
  $http.post('/user/login', {username: username, password: password})
    // handle success
    .success(function (data, status) {
      if(status === 200 && data.status){
        SessionService.set('auth',true); //This sets our session key/val pair as authenticated
        user = sessionStorage.getItem('auth');
        deferred.resolve();
      } else {
        user = false;
        deferred.reject();
      }
    })
    // handle error
    .error(function (data) {
      user = false;
      deferred.reject();
    });

  // return promise object
  return deferred.promise;

}

function logout() {

  // create a new instance of deferred
  var deferred = $q.defer();

  // send a get request to the server
  $http.get('/user/logout')
    // handle success
    .success(function (data) {
      SessionService.unset('auth');
      user = false;
      deferred.resolve();
    })
    // handle error
    .error(function (data) {
      user = false;
      deferred.reject();
    });

  // return promise object
  return deferred.promise;

}

function register(username, password) {

  // create a new instance of deferred
  var deferred = $q.defer();

  // send a post request to the server
  $http.post('/user/register', {username: username, password: password})
    // handle success
    .success(function (data, status) {
      if(status === 200 && data.status){
        deferred.resolve();
      } else {
        deferred.reject();
      }
    })
    // handle error
    .error(function (data) {
      deferred.reject();
    });

  // return promise object
  return deferred.promise;

}

}]);

login.factory('SessionService',function(){
   return{
       get:function(key){
           return sessionStorage.getItem(key);
       },
       set:function(key,val){
           return sessionStorage.setItem(key,val);
       },
       unset:function(key){
           return sessionStorage.removeItem(key);
       }
   }
});